class DocumentsController < ApplicationController

	def new
		@document  = Document.new
	end

	def create
		doc = params[:document]
		@document = Document.new(
			title: doc[:title],
			document_attachment: doc[:document_attachment])
		if @document.save
			redirect_to new_document_path
		else
			render 'new'
		end
	end
end
