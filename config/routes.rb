Rails.application.routes.draw do
	root to: 'static_pages#index'

	resources :static_pages, only: [:index] do
		get :search, on: :collection
	end
	resources :documents, only: [:new, :create]
end
